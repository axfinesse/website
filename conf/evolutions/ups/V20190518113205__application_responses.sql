ALTER TABLE player_applications
    ADD COLUMN user INT NOT NULL AFTER id,
    ADD COLUMN status INT NOT NULL, -- 0 for pending, 1 for rejected, 2 for accepted
    ADD COLUMN reject_message TEXT NULL,
    ADD COLUMN responded_by INT NULL,
    ADD COLUMN responded_at TIMESTAMP NULL,
    ADD FOREIGN KEY fk_player_application_rejecter (responded_by) REFERENCES users(id),
    ADD CHECK ( status >= 0 AND status <= 2 ),
    ADD CHECK ( status != 1 OR ( reject_message IS NOT NULL AND responded_by IS NOT NULL AND responded_at IS NOT NULL ) );

ALTER TABLE players
    DROP COLUMN approved,
    DROP COLUMN approved_by,
    DROP COLUMN approved_on;
