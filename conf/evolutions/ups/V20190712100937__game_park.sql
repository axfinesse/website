alter table games
    add column park int not null;

update games
    set park=(select park from teams where id=home_team);

alter table games
    modify park int not null,
    add foreign key fk_games_park (park) references parks(id);
