CREATE TABLE IF NOT EXISTS gm_assignments (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    team INT NOT NULL,
    gm INT NOT NULL,
    start_stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    end_stamp TIMESTAMP NULL,
    end_reason VARCHAR(30) NULL,
    FOREIGN KEY fk_gm_assignments_team (team) REFERENCES teams(id),
    FOREIGN KEY fk_gm_assignments_gm (gm) REFERENCES players(id)
);
