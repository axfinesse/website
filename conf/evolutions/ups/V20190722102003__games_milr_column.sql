alter table games
    add column milr boolean not null after session;

update games g
    set g.milr=(select t.milr from teams t where t.id=g.home_team);
