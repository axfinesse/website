ALTER TABLE games
    ADD COLUMN stats_calculated BOOLEAN NOT NULL DEFAULT TRUE;

ALTER TABLE batting_stats
    ADD COLUMN season INT NOT NULL AFTER player,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (player, season);

ALTER TABLE pitching_stats
    ADD COLUMN season INT NOT NULL AFTER player,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (player, season);
