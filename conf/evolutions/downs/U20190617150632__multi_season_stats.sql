ALTER TABLE games
    DROP COLUMN stats_calculated;

ALTER TABLE batting_stats
    DROP COLUMN season,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (player);

ALTER TABLE pitching_stats
    DROP COLUMN season,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (player);
