package model

import play.api.data._
import play.api.data.Forms._

case class EditTeamForm(tag: String, name: String, park: Int, colorDiscord: Int, colorRoster: Int, colorRosterBg: Int, milr: Boolean, milrTeam: Option[Int])

object EditTeamForm {

  def editTeamForm(implicit db: FBDatabase): Form[EditTeamForm] = Form(
    mapping(
      "tag" -> nonEmptyText(minLength = 2, maxLength = 3),
      "name" -> nonEmptyText(maxLength = 50),
      "park" -> number(min = 1).verifying("This is not a valid park.", db.getParkById(_).isDefined),
      "colorDiscord" -> number(min = 0, max = 0xFFFFFF),
      "colorRoster" -> number(min = 0, max = 0xFFFFFF),
      "colorRosterBg" -> number(min = 0, max = 0xFFFFFF),
      "milr" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "milrTeam" -> optional(number(min = 1).verifying("This is not a valid MiLR team.", db.getTeamById(_).exists(_.milr)))
    )(EditTeamForm.apply)(EditTeamForm.unapply)
      .verifying("You cannot give a MiLR team another MiLR team.", form => !form.milr || form.milrTeam.isEmpty)
  )

}
