package model

import play.api.data._
import play.api.data.Forms._

case class UserPreferencesForm(umpBatterPing: Option[String])

object UserPreferencesForm {

  val userPreferencesForm = Form(
    mapping(
      "umpBatterPing" -> optional(text)
    )(UserPreferencesForm.apply)(UserPreferencesForm.unapply)
  )

}
