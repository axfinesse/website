package model

import play.api.data._
import play.api.data.Forms._

case class DeleteActionForm(id: Int)

object DeleteActionForm {

  val deleteActionForm = Form(
    mapping(
      "id" -> number(min = 1)
    )(DeleteActionForm.apply)(DeleteActionForm.unapply)
  )

}
