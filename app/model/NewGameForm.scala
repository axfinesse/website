package model

import java.util.Date

import play.api.data.Forms._
import play.api.data._

case class NewGameForm(season: Int, session: Int, milr: Boolean, awayTeam: String, homeTeam: String, reddit_link: Option[String], startDate: Date, statsCalculation: Boolean, park: Option[Int]) {

  val id36: Option[String] = reddit_link.map {
    case NewGameForm.REDDIT_ID36_REGEX(matchedID36) => matchedID36
    case link => link
  }

  def validateTeamsAreFree()(implicit db: FBDatabase): Boolean = {
    db.getGameByTeamTag(season, session, awayTeam).isEmpty &&
      db.getGameByTeamTag(season, session, homeTeam).isEmpty
  }

}

object NewGameForm {

  private val REDDIT_ID36_REGEX = """.*?reddit\.com/r/fakebaseball/comments/([0-9a-zA-Z]{6,7})/.*""".r

  def newGameForm(implicit db: FBDatabase): Form[NewGameForm] = Form(
    mapping(
      "season" -> number(min = 1),
      "session" -> number(min = 0),
      "milr" -> default(boolean, false),
      "awayTeam" -> nonEmptyText(minLength = 2, maxLength = 3).verifying("Invalid away team.", db.getTeamByTag(_).isDefined),
      "homeTeam" -> nonEmptyText(minLength = 2, maxLength = 3).verifying("Invalid home team.", db.getTeamByTag(_).isDefined),
      "id36" -> optional(nonEmptyText.verifying("This doesn't look like a reddit link or ID36.", link => {
        link match {
          case REDDIT_ID36_REGEX(_*) => true
          case _ => (link.length == 6 || link.length == 7) && link.forall(_.isLetterOrDigit)
        }
      })),
      "startTime" -> date("yyyy-MM-dd"),
      "statsCalculation" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "park" -> optional(number(min = 1))
    )(NewGameForm.apply)(NewGameForm.unapply)
      .verifying("The home team cannot be the same as the away team.", form => form.awayTeam != form.homeTeam)
      .verifying("The away team is already scheduled for a game in this session.", form => db.getGameByTeamTag(form.season, form.session, form.awayTeam).isEmpty)
      .verifying("The home team is already scheduled for a game in this session.", form => db.getGameByTeamTag(form.season, form.session, form.homeTeam).isEmpty)
  )

}
