package services

import javax.inject.{Inject, Singleton}
import model._

@Singleton
class GlobalSettingsProvider @Inject()(db: FBDatabase) {

  type GlobalSettingName = String
  val INDEX_MESSAGE: GlobalSettingName = "index.message"
  val AB_PINGS_CHANNEL: GlobalSettingName = "discord.channel.ping"

  def getSetting(name: GlobalSettingName): Option[String] = {
    db.getGlobalSetting(name).map(_.value)
  }

  def setSetting(name: GlobalSettingName, value: String): Unit = {
    db.setGlobalSetting(new GlobalSetting(name, value))
  }

}
