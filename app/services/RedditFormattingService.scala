package services

import model._

object RedditFormattingService {

  def generateLineScore(allPlays: Seq[GameActionWithStatesAndPlayers], awayTeam: String, homeTeam: String, complete: Boolean): String = {

    // Always present
    var line1 = "||"
    var line2 = "|:--|"
    var line3 = s"|**$awayTeam**|"
    var line4 = s"|**$homeTeam**|"

    // Track max inning played
    val maxInning = if (allPlays.isEmpty) 0 else allPlays.map(_.afterState.inning).max

    // Loop all innings played, but at least 6
    for (i <- 1 to Math.max(maxInning, 12)) {
      // Determine if the current inning being added is the active inning
      val italics = i == maxInning && !complete
      // Add away score if odd, home score if even
      if (i % 2 == 1) {
        line1 += s"${(i + 1) / 2}|"
        line2 += ":--|"

        // Only add score content if we have reached that inning
        if (i <= maxInning)
          line3 += s"${if (italics) "*" else ""}${allPlays.filter(_.beforeState.inning == i).map(_.gameAction.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line3 += "|"
      } else {
        if (i <= maxInning)
          line4 += s"${if (italics) "*" else ""}${allPlays.filter(_.beforeState.inning == i).map(_.gameAction.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line4 += "|"
      }
    }

    line1 += "R|H|"
    line2 += ":--|:--|"
    line3 += s"${allPlays.headOption.map(_.afterState.scoreAway).getOrElse(0)}|${allPlays.count(p => p.beforeState.inning % 2 == 1 && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|"
    line4 += s"${allPlays.headOption.map(_.afterState.scoreHome).getOrElse(0)}|${allPlays.count(p => p.beforeState.inning % 2 == 0 && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|"

    // Return lines combined
    s"\n$line1\n$line2\n$line3\n$line4"
  }

  def generateLineupBox(allPlays: Seq[GameActionWithStatesAndPlayers], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], awayTeam: String, homeTeam: String): String = {
    val header = s"|\\#|$awayTeam|Pos|AB|R|H|RBI|BB|K|BA|\\#|$homeTeam|Pos|AB|R|H|RBI|BB|K|BA|\n" +
      "|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var lineupBox = ""
    for (i <- 1 to 9) {
      // Figure out how many rows we need for this batting position
      val aways = awayLineup.map(_.filter(_.lineupEntry.battingPos == i).sortBy(_.lineupEntry.id))
      val homes = homeLineup.map(_.filter(_.lineupEntry.battingPos == i).sortBy(_.lineupEntry.id))
      val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

      // Now start putting in the players, or blank lines if none
      for (j <- 0 until rows) {
        val awayPlayer = aways.flatMap(_.lift(j))
        lineupBox += awayPlayer.map(entry => {
          s"|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}**$i**${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
            s"${entry.lineupEntry.position}|${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.playType != PLAY_TYPE_STEAL && p.gameAction.playType != PLAY_TYPE_MULTI_STEAL && p.gameAction.playType != PLAY_TYPE_IBB && p.gameAction.playType != PLAY_TYPE_AUTO_BB && !p.gameAction.result.contains("BB")))}|" +
            s"${allPlays.count(_.gameAction.scorers.exists(_.split(",").map(_.toInt).contains(entry.player.player.id)))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
            s"${allPlays.filter(p => p.batter.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.runsScored)}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.playType == PLAY_TYPE_IBB || p.gameAction.playType == PLAY_TYPE_AUTO_BB || p.gameAction.result.contains("BB")))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
            s"${"%.3f".format(entry.player.battingStats.map(_.ba).getOrElse(0f))}|"
        }).getOrElse("|--|--|--|--|--|--|--|--|--|--|")
        val homePlayer = homes.flatMap(_.lift(j))
        lineupBox += homePlayer.map(entry => {
          s"${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}**$i**${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
            s"${entry.lineupEntry.position}|${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.playType != PLAY_TYPE_STEAL && p.gameAction.playType != PLAY_TYPE_MULTI_STEAL && p.gameAction.playType != PLAY_TYPE_IBB && p.gameAction.playType != PLAY_TYPE_AUTO_BB && !p.gameAction.result.contains("BB")))}|" +
            s"${allPlays.count(_.gameAction.scorers.exists(_.split(",").map(_.toInt).contains(entry.player.player.id)))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
            s"${allPlays.filter(p => p.batter.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.runsScored)}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.playType == PLAY_TYPE_IBB || p.gameAction.playType == PLAY_TYPE_AUTO_BB || p.gameAction.result.contains("BB")))}|" +
            s"${allPlays.count(p => p.batter.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
            s"${"%.3f".format(entry.player.battingStats.map(_.ba).getOrElse(0f))}|\n"
        }).getOrElse("|--|--|--|--|--|--|--|--|--|--|\n")
      }
    }

    // Return the combined string
    s"$header\n$lineupBox"
  }

  def generatePitcherBox(allPlays: Seq[GameActionWithStatesAndPlayers], awayLineup: Option[Seq[LineupEntryWithPlayer]], homeLineup: Option[Seq[LineupEntryWithPlayer]], awayTeam: String, homeTeam: String): String = {
    val header = s"|$awayTeam|IP|H|ER|BB|K|ERA|$homeTeam|IP|H|ER|BB|K|ERA|\n" +
      s"|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var pitcherBox = ""

    // Figure out how many rows we need for this table
    val aways = awayLineup.map(_.filter(_.lineupEntry.battingPos == 0).sortBy(_.lineupEntry.id))
    val homes = homeLineup.map(_.filter(_.lineupEntry.battingPos == 0).sortBy(_.lineupEntry.id))
    val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

    // Now start putting in the players, or blank lines if none
    for (j <- 0 until rows) {
      val awayPlayer = aways.flatMap(_.lift(j))
      pitcherBox += awayPlayer.map(entry => {
        s"|${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(allPlays.filter(p => p.pitcher.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
          s"${allPlays.filter(_.pitcher.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.runsScored)}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("BB") || p.gameAction.result.contains("Auto BB")))}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.map(_.era).getOrElse(0f))}|"
      }).getOrElse("|--|--|--|--|--|--|--|")
      val homePlayer = homes.flatMap(_.lift(j))
      pitcherBox += homePlayer.map(entry => {
        s"${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}[${entry.player.player.fullName}](${entry.player.user.redditName})${if (entry.lineupEntry.replacedBy.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(allPlays.filter(p => p.pitcher.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("HR") || p.gameAction.result.contains("3B") || p.gameAction.result.contains("2B") || p.gameAction.result.contains("1B")))}|" +
          s"${allPlays.filter(_.pitcher.id == entry.player.player.id).foldLeft(0)(_ + _.gameAction.runsScored)}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("BB") || p.gameAction.result.contains("Auto BB")))}|" +
          s"${allPlays.count(p => p.pitcher.id == entry.player.player.id && (p.gameAction.result.contains("K") || p.gameAction.result.contains("Auto K") || p.gameAction.result.contains("Bunt K")))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.map(_.era).getOrElse(0f))}|\n"
      }).getOrElse("--|--|--|--|--|--|--|\n")
    }

    // Combine and return
    s"$header\n$pitcherBox"
  }

  def generateScoringPlaysBox(scoringPlays: Seq[ScoringPlayWithActionAndStates]): String = {
    val header = "|Inning|Play|Score|\n|:--|:--|:--|"

    val plays = scoringPlays
      .sortBy(_.gameAction.id)
      .map(sp => s"|${sp.beforeState.inningStr}|${sp.scoringPlay.description}|${sp.afterState.scoreAway} - ${sp.afterState.scoreHome}|")
      .mkString("\n")

    s"$header\n$plays"
  }

}
