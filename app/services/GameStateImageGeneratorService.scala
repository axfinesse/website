package services

import java.awt.{BasicStroke, Color, GradientPaint, Paint, Polygon}
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream

import javax.imageio.ImageIO
import javax.inject.{Inject, Singleton}
import model._
import play.api.Environment

@Singleton
class GameStateImageGeneratorService @Inject()(env: Environment) {

  def generateImage(game: GameWithGameState, battingLineup: Option[Seq[LineupEntryWithPlayer]], fieldingLineup: Option[Seq[LineupEntryWithPlayer]]): String = {
    // Load the template file
    val baseImgStream = env.resourceAsStream("public/images/diamond_template.svg").get
    val baos = new ByteArrayOutputStream()
    val buf = new Array[Byte](1024)
    var len = 0
    while ( {
      len = baseImgStream.read(buf); len
    } != -1)
      baos.write(buf, 0, len)

    // Copy the data into a string for replacing (SVG is plaintext anyway)
    baseImgStream.close()
    val svgData = new String(baos.toByteArray, "UTF-8")
    baos.close()

    def lastName: LineupEntryWithPlayer => String = _.player.player.lastName

    // Get all the fielders' names
    val pitcher = fieldingLineup.flatMap(_.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val catcher = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "C" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val firstBasemen = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "1B" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val secondBasemen = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "2B" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val thirdBasemen = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "3B" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val shortstop = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "SS" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val leftFielder = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "LF" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val centerFielder = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "CF" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")
    val rightFielder = fieldingLineup.flatMap(_.find(e => e.lineupEntry.position == "RF" && e.lineupEntry.replacedBy.isEmpty)).map(lastName).getOrElse("")

    // Get the batter and baserunners
    val nextBatter = battingLineup.flatMap(_.find(e => e.lineupEntry.battingPos == game.nextBatterLineupPos && e.lineupEntry.replacedBy.isEmpty))
    val runnerOnFirst = game.state.r1.flatMap(p => battingLineup.flatMap(_.find(_.player.player.id == p))).map(lastName).getOrElse("")
    val runnerOnSecond = game.state.r2.flatMap(p => battingLineup.flatMap(_.find(_.player.player.id == p))).map(lastName).getOrElse("")
    val runnerOnThird = game.state.r3.flatMap(p => battingLineup.flatMap(_.find(_.player.player.id == p))).map(lastName).getOrElse("")

    // Replace all the placeholders and return
    svgData
      .replace("$PLAYER_P$", pitcher)
      .replace("$PLAYER_C$", catcher)
      .replace("$PLAYER_1B$", firstBasemen)
      .replace("$PLAYER_2B$", secondBasemen)
      .replace("$PLAYER_3B$", thirdBasemen)
      .replace("$PLAYER_SS$", shortstop)
      .replace("$PLAYER_LF$", leftFielder)
      .replace("$PLAYER_CF$", centerFielder)
      .replace("$PLAYER_RF$", rightFielder)
      .replace("$PLAYER_R1$", runnerOnFirst)
      .replace("$PLAYER_R2$", runnerOnSecond)
      .replace("$PLAYER_R3$", runnerOnThird)
      .replace("$1BFILL$", if (game.state.r1.isDefined) "black" else "white")
      .replace("$2BFILL$", if (game.state.r2.isDefined) "black" else "white")
      .replace("$3BFILL$", if (game.state.r3.isDefined) "black" else "white")
      .replace("$PLAYER_BL$", nextBatter.filterNot(_.player.player.righthanded).map(lastName).getOrElse(""))
      .replace("$PLAYER_BR$", nextBatter.filter(_.player.player.righthanded).map(lastName).getOrElse(""))
  }

  private val HOME_PLATE_POLYGON = new Polygon(Array(80, 120, 120, 100, 80), Array(150, 150, 170, 190, 170), 5)
  private val FIRST_BASE_POLYGON = new Polygon(Array(190, 160, 130, 160), Array(100, 70, 100, 130), 4)
  private val SECOND_BASE_POLYGON = new Polygon(Array(100, 70, 100, 130), Array(10, 40, 70, 40), 4)
  private val THIRD_BASE_POLYGON = new Polygon(Array(10, 40, 70, 40), Array(100, 70, 100, 130), 4)

  def generateBasicImage(game: GameWithGameState): Array[Byte] = {
    // Create empty image
    val img = new BufferedImage(600, 300, BufferedImage.TYPE_INT_ARGB)
    val g = img.createGraphics()

    // Background
    g.setColor(Color.white)
    g.setPaint(new GradientPaint(0f, 0f, new Color(game.awayTeam.colorRosterBg), 600f, 300f, new Color(game.homeTeam.colorRosterBg)))
    g.fillRect(0, 0, 600, 300)

    // Away logo
    val awayImgStream = env.resourceAsStream(s"public/images/teams/${game.awayTeam.tag}.png")
    awayImgStream.foreach { awayImgStream =>
      val awayImg = ImageIO.read(awayImgStream)
      g.drawImage(awayImg, 50, 50, 200, 200, null)
      awayImgStream.close()
    }

    // Home logo
    val homeImgStream = env.resourceAsStream(s"public/images/teams/${game.homeTeam.tag}.png")
    homeImgStream.foreach { homeImgStream =>
      val homeImg = ImageIO.read(homeImgStream)
      g.drawImage(homeImg, 350, 50, 200, 200, null)
      homeImgStream.close()
    }

    // Write to byte arry
    val baos = new ByteArrayOutputStream()
    ImageIO.write(img, "png", baos)
    baos.toByteArray
  }

}
