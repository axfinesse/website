package controllers

import javax.inject.{Inject, Singleton}
import model.{FBDatabase, Tables}
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{GlobalSettingsProvider, MarkdownFormatterService}

@Singleton
class HomeController @Inject()(markdownService: MarkdownFormatterService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = messagesActionBuilder { implicit req =>
    implicit val iUser: Option[Tables.UsersRow] = user
    val message = markdownService.parseMarkdown(settingsProvider.getSetting(settingsProvider.INDEX_MESSAGE).getOrElse("No message provided."))
    Ok(views.html.index(message))
  }

}
