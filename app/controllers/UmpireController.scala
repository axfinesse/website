package controllers

import javax.inject.{Inject, Singleton}
import model.DeleteActionForm._
import model.LineupForm._
import model.NewGameActionForm._
import model.SetID36Form._
import model.EditScoringPlayForm._
import model.GameAwardsForm._
import model._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{DiscordMessageService, GlobalSettingsProvider, MLRCalculatorService, RedditFormattingService}

@Singleton
class UmpireController @Inject()(calc: MLRCalculatorService, discordMessageService: DiscordMessageService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    val games = db.getGamesForUmpire(ru)
    Ok(views.html.umpires.index(games))
  }

  private def generateBatterPing(game: GameWithGameState, batter: LineupEntryWithPlayer, allPlays: Seq[GameActionWithStatesAndPlayers], batterPingFormat: String): String = {
    var hits = 0
    var abs = 0
    var plays = Seq[String]()
    allPlays.reverse.foreach { play =>
      if (play.gameAction.batter.contains(batter.player.player.id)) {
        // Count hits/ABs
        if (play.gameAction.result.contains("HR") || play.gameAction.result.contains("3B") || play.gameAction.result.contains("2B") || play.gameAction.result.contains("1B")) {
          hits += 1
          abs += 1
        } else if (play.gameAction.result.contains("FO") || play.gameAction.result.contains("K") || play.gameAction.result.contains("Bunt K") || play.gameAction.result.contains("Auto K") || play.gameAction.result.contains("PO") || play.gameAction.result.contains("RGO") || play.gameAction.result.contains("LGO")) {
          abs += 1
        }

        // Make list of plays
        val i = play.beforeState.inning
        val inning = if (i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}"
        plays :+= s"${play.gameAction.result.get} in $inning"
      }
    }

    val awayTag = game.awayTeam.tag
    val homeTag = game.homeTeam.tag
    val awayScore = game.state.scoreAway.toString
    val homeScore = game.state.scoreHome.toString
    val basesDiamonds = s" ${if (game.state.r3.isDefined) "\u25C6" else "\u25C7"} ^${if (game.state.r2.isDefined) "\u25C6" else "\u25C7"} ${if (game.state.r1.isDefined) "\u25C6" else "\u25C7"}"
    val inning = game.inning
    val outs = game.state.outs.toString
    val batterTeamTag = if (game.awayBatting) game.awayTeam.tag else game.homeTeam.tag
    val batterPosition = batter.lineupEntry.position
    val batterPing = s"[${batter.player.player.fullName}](${batter.player.user.redditName})"
    val batterRecord = s"$hits-$abs${if (plays.nonEmpty) s": ${plays.mkString(", ")}" else ""}"

    batterPingFormat
      .replace("%awayTag%", awayTag)
      .replace("%homeTag%", homeTag)
      .replace("%awayScore%", awayScore)
      .replace("%homeScore%", homeScore)
      .replace("%basesDiamonds%", basesDiamonds)
      .replace("%inning%", inning)
      .replace("%outs%", outs)
      .replace("%batterTeamTag%", batterTeamTag)
      .replace("%batterPosition%", batterPosition)
      .replace("%batterPing%", batterPing)
      .replace("%batterRecord%", batterRecord)
  }

  def showNewGameActionForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).map { game =>
      game.fieldingLineup.map(db.getLineupWithPlayers(_, game.game)).flatMap { fieldingLineup =>
        game.battingLineup.map(db.getLineupWithPlayers(_, game.game)).map { battingLineup =>

          // Get the current batter and pitcher to show on form
          val nextBatter = battingLineup.find(e => e.lineupEntry.battingPos == game.nextBatterLineupPos && e.lineupEntry.replacedBy.isEmpty).get
          val pitcher = fieldingLineup.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty).get

          // Generate the batter ping
          val pingFormat = db.getUserWithPreferencesById(ru._2.id).get.preferences.withDefaults.umpBatterPing.get
          val batterPing = generateBatterPing(game, nextBatter, db.getGameLog(game.game), pingFormat)

          // Display form
          val potentialStealers = Seq(game.state.r1, game.state.r2, game.state.r3)
            .filter(_.isDefined)
            .map(p => db.getPlayerById(p.get).get)

          val nBatter = db.getPlayerWithTypes(nextBatter.player.player.id).get
          val nPitcher = db.getPlayerWithTypes(pitcher.player.player.id).get

          val discordPingMessage =
            if (game.game.id36.isEmpty) Some("Set Reddit Thread First")
            else if (nextBatter.player.user.discord.isEmpty) Some("Player Is Not Discord Verified")
            else if (db.getDiscordPing(nextBatter.player.user, game.game, game.state).isDefined) Some("Already Pinged")
            else None

          Ok(views.html.umpires.newaction(gameId, discordPingMessage, nBatter, potentialStealers, nPitcher, newGameActionForm, "", batterPing, None))

        }
      } getOrElse {
        BadRequest("Please submit the lineups before starting the game.")
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitNewGameActionForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).map { game =>
      game.fieldingLineup.map(db.getLineupWithPlayers(_, game.game)).flatMap { fieldingLineup =>
        game.battingLineup.map(db.getLineupWithPlayers(_, game.game)).map { battingLineup =>

          // Get pitcher and batter to make sure they're correct
          val nextBatter = battingLineup.find(e => e.lineupEntry.battingPos == game.nextBatterLineupPos && e.lineupEntry.replacedBy.isEmpty).get
          val pitcher = fieldingLineup.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty).get

          val potentialStealers = Seq(game.state.r1, game.state.r2, game.state.r3)
            .filter(_.isDefined)
            .map(p => db.getPlayerById(p.get).get)

          val pingFormat = db.getUserWithPreferencesById(ru._2.id).get.preferences.withDefaults.umpBatterPing.get
          val batterPing = generateBatterPing(game, nextBatter, db.getGameLog(game.game), pingFormat)

          val nBatter = db.getPlayerWithTypes(nextBatter.player.player.id).get
          val nPitcher = db.getPlayerWithTypes(pitcher.player.player.id).get

          val discordPingMessage =
            if (game.game.id36.isEmpty) Some("Set Reddit Thread First")
            else if (nextBatter.player.user.discord.isEmpty) Some("Player Is Not Discord Verified")
            else if (db.getDiscordPing(nextBatter.player.user, game.game, game.state).isDefined) Some("Already Pinged")
            else None

          newGameActionForm.bindFromRequest.fold(
            formWithErrors => BadRequest(views.html.umpires.newaction(gameId, discordPingMessage, nBatter, potentialStealers, nPitcher, formWithErrors, "", batterPing, None)),
            formData => {
              // Make sure batter and pitcher are correct (if submitted wrong, the form may have been submitted by another umpire)
              if (formData.playType != PLAY_TYPE_STEAL && formData.playType != PLAY_TYPE_MULTI_STEAL && formData.batter != nextBatter.player.player.id)
                BadRequest(views.html.umpires.newaction(gameId, discordPingMessage, nBatter, potentialStealers, nPitcher, newGameActionForm.withGlobalError("This is not the current batter. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing, None))
              else if ((formData.playType == PLAY_TYPE_STEAL || formData.playType == PLAY_TYPE_MULTI_STEAL) && formData.batter == nextBatter.player.player.id)
                BadRequest(views.html.umpires.newaction(gameId, discordPingMessage, nBatter, potentialStealers, nPitcher, newGameActionForm.withGlobalError("The batter cannot be the stealer. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing, None))
              else if (formData.pitcher != pitcher.player.player.id)
                BadRequest(views.html.umpires.newaction(gameId, discordPingMessage, nBatter, potentialStealers, nPitcher, newGameActionForm.withGlobalError("This is not the current pitcher. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing, None))
              else {
                var (newAction, newState, scoringPlay, minRange, maxRange): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = formData.playType match {
                  case PLAY_TYPE_SWING | model.PLAY_TYPE_INFIELD_IN => calc.handleSwing(game.state, nPitcher, formData.pitch.get, nBatter, formData.swing.get, game.park, formData.playType == model.PLAY_TYPE_INFIELD_IN)
                  case PLAY_TYPE_AUTO_K => calc.handleAutoK(game.state, nPitcher, nBatter)
                  case PLAY_TYPE_AUTO_BB => calc.handleAutoBB(game.state, nPitcher, nBatter)
                  case PLAY_TYPE_BUNT => calc.handleBunt(game.state, pitcher.player.player, formData.pitch.get, nextBatter.player.player, formData.swing.get)
                  case PLAY_TYPE_STEAL => calc.handleSteal(game.state, pitcher.player.player, formData.pitch.get, db.getPlayerById(formData.batter).get, formData.swing.get)
                  case PLAY_TYPE_MULTI_STEAL => calc.handleMultiSteal(game.state, pitcher.player.player, formData.pitch.get, db.getPlayerById(formData.batter).get, formData.swing.get)
                  case PLAY_TYPE_IBB => calc.handleIBB(game.state, nPitcher, nBatter)
                }

                if (formData.save) {
                  // Save the updated play in the database
                  newState = db.addGameAction(game.game, newAction, newState, scoringPlay)

                  // Check if we are ending the game or going to extras
                  var shouldEndGame = false
                  val inningFlipped = newState.inning != game.state.inning
                  if (newState.inning > 11) {
                    if (newState.inning % 2 == 0) { // Going into bottom
                      shouldEndGame = newState.scoreHome > newState.scoreAway
                    } else { // Going into top
                      shouldEndGame = inningFlipped && newState.scoreHome != newState.scoreAway
                    }

                    if (inningFlipped && !shouldEndGame && newState.inning > 12) { // Going into extras

                      // Determine runners
                      val awayBatting = newState.inning % 2 == 1
                      val nextBattingLineup = fieldingLineup // Have to flip for the inning switch not being reflected yet
                      var bPosBack1 = ((if (awayBatting) newState.awayBattingPosition else newState.homeBattingPosition) + 7) % 9 + 1
                      var bPosBack2 = ((if (awayBatting) newState.awayBattingPosition else newState.homeBattingPosition) + 6) % 9 + 1
                      var bPosBack3 = ((if (awayBatting) newState.awayBattingPosition else newState.homeBattingPosition) + 5) % 9 + 1
                      if (bPosBack1 < 1)
                        bPosBack1 = 9 - bPosBack1
                      if (bPosBack2 < 1)
                        bPosBack2 = 9 - bPosBack2
                      if (bPosBack3 < 1)
                        bPosBack3 = 9 - bPosBack3
                      val (onFirst, onSecond, onThird) = if (newState.inning > 16) { // Going into 9th, add three runners
                        (
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack3 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack2 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack1 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                        )
                      } else if (newState.inning > 14) { // Going into 8th, add two runners
                        (
                          None,
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack2 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack1 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                        )
                      } else { // Going into 7th, add one runner
                        (
                          None,
                          Some(nextBattingLineup.find(e => e.lineupEntry.battingPos == bPosBack1 && e.lineupEntry.replacedBy.isEmpty).get.lineupEntry.player),
                          None
                        )
                      }

                      // Add new runner-placing dummy action
                      db.updateStateForExtras(newState, onFirst, onSecond, onThird)
                    }
                  }

                  if (shouldEndGame) {
                    db.markGameComplete(gameId)
                    Redirect(routes.GameController.viewGame(gameId))
                  } else {
                    Redirect(routes.UmpireController.showNewGameActionForm(gameId))
                  }

                } else {
                  // Generate the preview and return to the form
                  var preview = formData.playType match {
                    case PLAY_TYPE_AUTO_K => "Auto-K"
                    case PLAY_TYPE_AUTO_BB => "Auto-BB"
                    case PLAY_TYPE_STEAL | PLAY_TYPE_MULTI_STEAL => "Steal"
                    case PLAY_TYPE_BUNT => "Bunt"
                    case PLAY_TYPE_IBB => "IBB"
                    case _ => "Swing"
                  }

                  preview += s": ${newAction.swing.map(_.toString).getOrElse("x")}  \n"
                  preview += s"Pitch: ${newAction.pitch.map(_.toString).getOrElse("x")}  \n"
                  preview += s"Diff: ${newAction.diff.map(_.toString).getOrElse("x")} -> "
                  if (newAction.result.contains("FO") && newAction.runsScored == 1) {
                    preview += "Sac"
                  } else if (newAction.outsTracked == 2) {
                    preview += s"${newAction.result.getOrElse("Unknown")} (DP)"
                  } else if (newAction.outsTracked == 3) {
                    preview += s"${newAction.result.getOrElse("Unknown")} (TP)"
                  } else {
                    preview += newAction.result.getOrElse("Unknown")
                  }

                  val rangeString = minRange.map(minRange => s"${newAction.result.get}: $minRange - ${maxRange.get}")
                  Ok(views.html.umpires.newaction(gameId, discordPingMessage, nBatter, potentialStealers, nPitcher, newGameActionForm.fill(formData), preview, batterPing, rangeString))
                }
              }
            }
          )
        }
      } getOrElse {
        UnauthorizedPage
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showLineupForm(gameId: Int, team: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGameWithInfoById(gameId).flatMap { game =>
      (if (game.awayTeam.tag == team) Some(game.awayTeam) else if (game.homeTeam.tag == team) Some(game.homeTeam) else None).map { team =>
        if (db.getGamesForUmpire(ru).exists(g => !g.game.completed && g.game.id == gameId)) {
          var form = lineupForm(team)
          (if (game.awayTeam.id == team.id) game.game.awayLineup.map(db.getLineupWithPlayers(_, game.game)) else if (game.homeTeam.id == team.id) game.game.homeLineup.map(db.getLineupWithPlayers(_, game.game)) else None).foreach { lineup =>
            form = form.bind(
              lineup
                .filter(e => e.lineupEntry.replacedBy.isEmpty)
                .flatMap { entry =>
                  if (entry.lineupEntry.battingPos == 0)
                    Map("pitcher" -> entry.lineupEntry.player.toString)
                  else
                    Map(
                      s"player${entry.lineupEntry.battingPos}" -> entry.lineupEntry.player.toString,
                      s"p${entry.lineupEntry.battingPos}Pos" -> entry.lineupEntry.position
                    )
                }.toMap
            )
          }
          Ok(views.html.umpires.editlineup(game.game, team, db.getPlayersOnTeam(team), form))
        }
        else
          UnauthorizedPage
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitLineupForm(gameId: Int, team: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit ru =>
    db.getGameWithInfoById(gameId).flatMap { game =>
      (if (game.awayTeam.tag == team) Some(game.awayTeam) else if (game.homeTeam.tag == team) Some(game.homeTeam) else None).map { team =>
        if (db.getGamesForUmpire(ru).exists(g => !g.game.completed && g.game.id == gameId))
          lineupForm(team).bindFromRequest().fold(
            formWithErrors => BadRequest(views.html.umpires.editlineup(game.game, team, db.getPlayersOnTeam(team), formWithErrors)),
            formData => {
              val currentLineup = if (game.awayTeam.id == team.id) game.game.awayLineup.map(db.getLineupWithPlayers(_, game.game)) else game.game.homeLineup.map(db.getLineupWithPlayers(_, game.game))
              if (currentLineup.isDefined)
                currentLineup.foreach { lineup =>
                  // Update lineup
                  val tupled = formData.tupled
                  val oldPitcher = lineup.find(e => e.lineupEntry.battingPos == 0 && e.lineupEntry.replacedBy.isEmpty).get
                  if (oldPitcher.player.player.id != formData.pitcher)
                    db.addLineupEntry(oldPitcher.lineupEntry.lineup, formData.pitcher, "P", 0, Some(oldPitcher.lineupEntry.id))
                  for (i <- tupled.indices) {
                    val oldEntry = lineup.find(e => e.lineupEntry.battingPos == (i + 1) && e.lineupEntry.replacedBy.isEmpty).get
                    if (oldEntry.lineupEntry.position == "PH" && oldEntry.player.player.id == tupled(i)._1)
                      db.replaceLineupEntryPosition(oldEntry.lineupEntry, tupled(i)._2)
                    else if (oldEntry.player.player.id != tupled(i)._1 || oldEntry.lineupEntry.position != tupled(i)._2)
                      db.addLineupEntry(oldEntry.lineupEntry.lineup, tupled(i)._1, tupled(i)._2, i + 1, Some(oldEntry.lineupEntry.id))
                  }
                }
              else {
                // New lineup
                val newLineup = db.createLineup
                val tupled = formData.tupled
                db.addLineupEntry(newLineup.id, formData.pitcher, "P", 0)
                for (i <- tupled.indices) {
                  db.addLineupEntry(newLineup.id, tupled(i)._1, tupled(i)._2, i + 1)
                }
                db.setLineup(game.game, newLineup, game.awayTeam.id == team.id)
              }
              Redirect(routes.GameController.viewGame(gameId))
            }
          )
        else
          UnauthorizedPage
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showDeleteRecentPlayForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).flatMap { _ =>
      db.getRecentGameActionWithPlayers(gameId).map { recentPlay =>
        Ok(views.html.umpires.deleteaction(recentPlay, deleteActionForm))
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def deleteRecentPlay(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).flatMap { _ =>
      db.getRecentGameActionWithPlayers(gameId).map { recentPlay =>
        deleteActionForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.umpires.deleteaction(recentPlay, formWithErrors)),
          formData =>
            if (formData.id != recentPlay.gameAction.id)
              BadRequest(views.html.umpires.deleteaction(recentPlay, deleteActionForm))
            else {
              db.deleteGameAction(recentPlay.gameAction)
              Redirect(routes.GameController.viewGame(gameId))
            }
        )
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def generateBoxScore(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGameWithInfoById(gameId).map { game =>
      // Get everything needed
      val allPlays = db.getGameLog(game.game)
      val awayLineup = game.game.awayLineup.map(id => db.getLineupWithPlayers(id, game.game))
      val homeLineup = game.game.homeLineup.map(id => db.getLineupWithPlayers(id, game.game))
      val awayScore = game.state.scoreAway
      val homeScore = game.state.scoreHome

      // Generate the harder parts
      val line = RedditFormattingService.generateLineScore(allPlays, game.awayTeam.tag, game.homeTeam.tag, game.game.completed)
      val box = RedditFormattingService.generateLineupBox(allPlays, awayLineup, homeLineup, game.awayTeam.name, game.homeTeam.name)
      val pitchers = RedditFormattingService.generatePitcherBox(allPlays, awayLineup, homeLineup, game.awayTeam.name, game.homeTeam.name)
      val scoringPlays = RedditFormattingService.generateScoringPlaysBox(db.getScoringPlaysWithActionsAndStates(game.game))

      // Put it all together
      val boxScore = s"##${game.awayTeam.tag} $awayScore - $homeScore ${game.homeTeam.tag}\n\n[View on Redditball](${routes.GameController.viewGame(gameId).absoluteURL()})\n\n##LINE\n$line\n\n##BOX\n$box\n\n##PITCHERS\n$pitchers\n\n##SCORING PLAYS\n$scoringPlays"
      Ok(views.html.umpires.boxscore(gameId, game.name, boxScore))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showRedditThreadForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGameWithInfoById(gameId).filter(g => ru.isCommissioner || db.getUmpiresForGame(g.game.id).exists(_.user.id == ru._2.id)).map { game =>
      Ok(views.html.umpires.setid36(GameWithTeams(game.game, game.awayTeam, game.homeTeam, game.park), game.game.id36.map(id36 => setID36Form.fill(SetID36Form(id36))).getOrElse(setID36Form)))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitRedditThreadForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGameWithInfoById(gameId).filter(g => ru.isCommissioner || db.getUmpiresForGame(g.game.id).exists(_.user.id == ru._2.id)).map { game =>
      setID36Form.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.umpires.setid36(GameWithTeams(game.game, game.awayTeam, game.homeTeam, game.park), formWithErrors)),
        formData => {
          db.setID36(game.game, formData.id36)
          Redirect(routes.GameController.viewGame(game.game.id))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showEditScoringPlaysForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).map { game =>
      Ok(views.html.umpires.editscoringplays(game, db.getScoringPlays(game.game), editScoringPlayForm))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitEditScoringPlayForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGamesForUmpire(ru).find(_.game.id == gameId).map { game =>
      editScoringPlayForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.umpires.editscoringplays(game, db.getScoringPlays(game.game), formWithErrors)),
        formData => {
          db.setScoringPlayDescription(gameId, formData.gameAction, formData.newDescription)
          Redirect(routes.UmpireController.showEditScoringPlaysForm(gameId))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showGameAwardsForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGameById(gameId).filter(g => ru.isCommissioner || db.getUmpiresForGame(g.id).exists(_.user.id == ru._2.id)).map { implicit game =>
      val existingAwards = db.getGameAwards(game)
      if (existingAwards.isEmpty || ru.isCommissioner) {
        val form = gameAwardsForm
            .bind(Map(
              "winningPitcher" -> existingAwards.map(_.winningPitcher.toString).getOrElse(""),
              "losingPitcher" -> existingAwards.map(_.losingPitcher.toString).getOrElse(""),
              "playerOfTheGame" -> existingAwards.map(_.playerOfTheGame.toString).getOrElse(""),
              "save" -> existingAwards.flatMap(_.save.map(_.toString)).getOrElse(""),
            ))
            .discardingErrors
        Ok(views.html.umpires.gameawards(game, form, db.getLineupWithPlayers(game.awayLineup.get, game) ++ db.getLineupWithPlayers(game.homeLineup.get, game)))
      } else {
        UnauthorizedPage
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitGameAwardsForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGameById(gameId).filter(g => ru.isCommissioner || db.getUmpiresForGame(g.id).exists(_.user.id == ru._2.id)).map { implicit game =>
      val existingAward = db.getGameAwards(game)
      if (existingAward.isDefined) {
        if (!ru.isCommissioner)
          UnauthorizedPage
        else
          gameAwardsForm.bindFromRequest.fold(
            formWithErrors => BadRequest(views.html.umpires.gameawards(game, formWithErrors, db.getLineupWithPlayers(game.awayLineup.get, game) ++ db.getLineupWithPlayers(game.homeLineup.get, game))),
            formData => {
              db.editGameAwards(gameId, formData)
              Redirect(routes.GameController.viewGame(gameId))
            }
          )
      } else {
        gameAwardsForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.umpires.gameawards(game, formWithErrors, db.getLineupWithPlayers(game.awayLineup.get, game) ++ db.getLineupWithPlayers(game.homeLineup.get, game))),
          formData => {
            db.addGameAwards(gameId, formData)
            Redirect(routes.GameController.viewGame(gameId))
          }
        )
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def sendBatterPing(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit ru =>
    db.getGameWithInfoById(gameId).filter(g => ru.isCommissioner || db.getUmpiresForGame(g.game.id).exists(_.user.id == ru._2.id)).map { game =>
      val battingLineup = db.getLineupWithPlayers(game.battingLineup.get, game.game)
      val batter = battingLineup.find(p => p.lineupEntry.battingPos == game.nextBatterLineupPos && p.lineupEntry.replacedBy.isEmpty).get
      val playerWithUser = PlayerWithUser(batter.player.player, batter.player.user)
      discordMessageService.sendABAlert(playerWithUser, game.game)
      db.addDiscordPing(ru, playerWithUser.user, game.game, game.state)
      Redirect(routes.UmpireController.showNewGameActionForm(gameId))
    } getOrElse {
      UnauthorizedPage
    }
  }

}
