package controllers

import akka.util.ByteString
import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.http.HttpEntity
import play.api.mvc._
import services.{GameStateImageGeneratorService, GlobalSettingsProvider}

@Singleton
class GameController @Inject()(gameStateImageGenerator: GameStateImageGeneratorService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = messagesActionBuilder { implicit req =>
    val maxSession = db.getMaxSession
    Redirect(routes.GameController.list(maxSession._1, maxSession._2))
  }

  def list(season: Int, session: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.games.list(db.getGamesInSession(season, session), Seq(), (season, session), db.getSessionCounts)(req, user))
  }

  def viewGame(gameId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getGameWithInfoById(gameId).map { game =>
      val umps = db.getUmpiresForGame(gameId)
      val editable = user.exists(u => u.isCommissioner || umps.exists(_.user.id == u.id))
      val umpNames = umps.map(up => up.player.map(_.fullName).getOrElse(up.user.redditName))
      val scoringPlays = db.getScoringPlays(game.game)
      val gameAwards = db.getGameAwards(game.game)
      Ok(views.html.games.view(game, if (game.game.completed) db.getGameLog(game.game).reverse else db.getGameLog(game.game), scoringPlays, game.game.awayLineup.map(db.getLineupWithPlayers(_, game.game)), game.game.homeLineup.map(db.getLineupWithPlayers(_, game.game)), editable, umpNames, gameAwards)(req, user))
    } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

  def getGameImage(gameId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getGameWithInfoById(gameId).map { game =>
      Result(
        header = ResponseHeader(200),
        body = HttpEntity.Strict(ByteString(gameStateImageGenerator.generateImage(game, game.battingLineup.map(db.getLineupWithPlayers(_, game.game)), game.fieldingLineup.map(db.getLineupWithPlayers(_, game.game)))), Some("image/svg+xml"))
      )
    } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

  def getGamePreviewImage(gameId: Int): Action[AnyContent] = messagesActionBuilder {
    db.getGameWithInfoById(gameId).map { game =>
      Result(
        header = ResponseHeader(200),
        body = HttpEntity.Strict(ByteString(gameStateImageGenerator.generateBasicImage(game)), Some("image/png"))
      )
    } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

}
